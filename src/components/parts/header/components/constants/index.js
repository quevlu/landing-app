// Obtener el lenguage si es que esta seteado y de estarlo comprobar si esta habilitado, sino setear uno por defecto

const AVAILABLE_LANGS = [
	{ value: 'es', text: 'Español' },
	{ value: 'en', text: 'Ingles' }
];

const LINKS = [
	{ href: '#home', title: 'Home', isAnchor: true },
	{ href: '#areas', title: 'Areas', isAnchor: true },
	{ href: '#characteristics', title: 'Caracteristicas', isAnchor: true },
	{ href: '#plans', title: 'Planes', isAnchor: true },
	{ href: '#', title: 'Centro de ayuda', isAnchor: false }
];

export { AVAILABLE_LANGS, LINKS };
