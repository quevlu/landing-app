const PERMISSIONS = {
	qrs: 'qrs',
	bar_codes: 'bar_codes',
	scanner: 'scanner',
	export: 'export',
	massive_print_tags: 'massive_print_tags',
	massive_upload_users: 'massive_upload_users',
	massive_upload_equipments: 'massive_upload_equipments',
	audits: 'audits',
	support: 'support'
};

const USERS = 'users';
const SECTORS = 'sectors';
const EQUIPMENTS = 'equipments';

const BASIC_LIMIT = 500;
const STANDARD_LIMIT = 1000;
const PREMIUM_LIMIT = 2000;
const ENTERPRISE_LIMIT = 3000;

const PLANS = [
	{
		name: 'Basico',
		description: 'Lorem ipsum generator dolorLorem ipsum generator dolor',
		price: 9,
		discountLot: 0.5,
		primary: false,
		characteristics: [
			{
				entity: USERS,
				quantity: BASIC_LIMIT
			},
			{
				entity: SECTORS,
				quantity: BASIC_LIMIT
			},
			{
				entity: EQUIPMENTS,
				quantity: BASIC_LIMIT
			}
		],
		features: [
			{
				key: PERMISSIONS.qrs,
				value: true
			},
			{
				key: PERMISSIONS.bar_codes,
				value: true
			},
			{
				key: PERMISSIONS.scanner,
				value: true
			},
			{
				key: PERMISSIONS.export,
				value: true
			},
			{
				key: PERMISSIONS.massive_print_tags,
				value: true
			},
			{
				key: PERMISSIONS.massive_upload_users,
				value: false
			},
			{
				key: PERMISSIONS.massive_upload_equipments,
				value: false
			},
			{
				key: PERMISSIONS.support,
				value: false
			},
			{
				key: PERMISSIONS.audits,
				value: false
			}
		]
	},
	{
		name: 'Estandar',
		description: 'Lorem ipsum generator dolorLorem ipsum generator dolor',
		price: 15,
		discountLot: 0.4,
		primary: false,
		characteristics: [
			{
				entity: USERS,
				quantity: STANDARD_LIMIT
			},
			{
				entity: SECTORS,
				quantity: STANDARD_LIMIT
			},
			{
				entity: EQUIPMENTS,
				quantity: STANDARD_LIMIT
			}
		],
		features: [
			{
				key: PERMISSIONS.qrs,
				value: true
			},
			{
				key: PERMISSIONS.bar_codes,
				value: true
			},
			{
				key: PERMISSIONS.scanner,
				value: true
			},
			{
				key: PERMISSIONS.export,
				value: true
			},
			{
				key: PERMISSIONS.massive_print_tags,
				value: true
			},
			{
				key: PERMISSIONS.massive_upload_users,
				value: true
			},
			{
				key: PERMISSIONS.massive_upload_equipments,
				value: false
			},
			{
				key: PERMISSIONS.support,
				value: false
			},
			{
				key: PERMISSIONS.audits,
				value: false
			}
		]
	},
	{
		name: 'Premium',
		description: 'Lorem ipsum generator dolorLorem ipsum generator dolor',
		price: 30,
		discountLot: 0.3,
		primary: true,
		characteristics: [
			{
				entity: USERS,
				quantity: PREMIUM_LIMIT
			},
			{
				entity: SECTORS,
				quantity: PREMIUM_LIMIT
			},
			{
				entity: EQUIPMENTS,
				quantity: PREMIUM_LIMIT
			}
		],
		features: [
			{
				key: PERMISSIONS.qrs,
				value: true
			},
			{
				key: PERMISSIONS.bar_codes,
				value: true
			},
			{
				key: PERMISSIONS.scanner,
				value: true
			},
			{
				key: PERMISSIONS.export,
				value: true
			},
			{
				key: PERMISSIONS.massive_print_tags,
				value: true
			},
			{
				key: PERMISSIONS.massive_upload_users,
				value: true
			},
			{
				key: PERMISSIONS.massive_upload_equipments,
				value: true
			},
			{
				key: PERMISSIONS.support,
				value: true
			},
			{
				key: PERMISSIONS.audits,
				value: false
			}
		]
	},
	{
		name: 'Enterprise',
		description: 'Lorem ipsum generator dolorLorem ipsum generator dolor',
		price: 50,
		discountLot: 0.2,
		primary: true,
		characteristics: [
			{
				entity: USERS,
				quantity: ENTERPRISE_LIMIT
			},
			{
				entity: SECTORS,
				quantity: ENTERPRISE_LIMIT
			},
			{
				entity: EQUIPMENTS,
				quantity: ENTERPRISE_LIMIT
			}
		],
		features: [
			{
				key: PERMISSIONS.qrs,
				value: true
			},
			{
				key: PERMISSIONS.bar_codes,
				value: true
			},
			{
				key: PERMISSIONS.scanner,
				value: true
			},
			{
				key: PERMISSIONS.export,
				value: true
			},
			{
				key: PERMISSIONS.massive_print_tags,
				value: true
			},
			{
				key: PERMISSIONS.massive_upload_users,
				value: true
			},
			{
				key: PERMISSIONS.massive_upload_equipments,
				value: true
			},
			{
				key: PERMISSIONS.support,
				value: true
			},
			{
				key: PERMISSIONS.audits,
				value: true
			}
		]
	}
];

const STEP = 1000;
const MULTIPLIER = 5;
const DISCOUNT = 0.3;

export { PLANS, STEP, MULTIPLIER, DISCOUNT };
