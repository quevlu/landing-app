const AREAS = [
	{
		icon: 'people',
		title: 'Usuarios',
		text: "Break big projects up into management 'sprints' that keep your team focused and productive."
	},
	{
		icon: 'business',
		title: 'Sectores',
		text: "Break big projects up into management 'sprints' that keep your team focused and productive."
	}
];

const IMAGES = [
	{ area: 'it', isHorizontal: true },
	{ area: 'tool', isHorizontal: false },
	{ area: 'warehouse', isHorizontal: true }
];

export { AREAS, IMAGES };
