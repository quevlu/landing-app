export default [
	{
		icon: 'qr-code',
		title: 'Qrs',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'barcode',
		title: 'Barcode',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'phone-portrait',
		title: 'Escaneo',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'print',
		title: 'Impresines',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'stats-chart',
		title: 'Estadisticas',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'people',
		title: 'Roles',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'analytics',
		title: 'Trackeo',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'download',
		title: 'Exportación',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'cloud-upload',
		title: 'Carga masiva',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'chatbox-ellipses',
		title: 'Asistencia',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'clipboard',
		title: 'Auditorias',
		text: 'Posibilidad de escanear qrs'
	},
	{
		icon: 'cash',
		title: 'Total',
		text: 'Posibilidad de escanear qrs'
	}
];
