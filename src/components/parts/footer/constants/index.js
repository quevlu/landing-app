const SOCIAL_NETWORKS = [
	{
		img: 'facebook',
		href: '#'
	},
	{
		img: 'instagram',
		href: '#'
	},
	{
		img: 'linkedin',
		href: '#'
	}
];

const SECTIONS = [
	{
		title: 'Website',
		items: [
			{ href: '#', title: 'Home', isAnchor: true },
			{ href: '#areas', title: 'Areas', isAnchor: true },
			{ href: '#characteristics', title: 'Caracteristicas', isAnchor: true },
			{ href: '#plans', title: 'Planes', isAnchor: true }
		]
	},
	{
		title: 'Acceso',
		items: [
			{ href: '#', title: 'Registrarse', isAnchor: false },
			{ href: '#', title: 'Ingresar', isAnchor: false }
		]
	},
	{
		title: 'Legal',
		items: [
			{ href: '#', title: 'Privacidad', isAnchor: false },
			{ href: '#', title: 'Terminos', isAnchor: false }
		]
	},
	{
		title: 'Ayuda',
		items: [{ href: '#', title: 'Centro de ayuda', isAnchor: false }]
	}
];

export { SOCIAL_NETWORKS, SECTIONS };
