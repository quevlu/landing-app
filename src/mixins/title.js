const title = {
	created() {},
	methods: {
		setTitle() {
			document.title = this.$t('common.title') + ' | Binaccle';
		}
	}
};

export default title;
