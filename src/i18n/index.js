import Vue from 'vue';
import VueI18n from 'vue-i18n';

const ls = require('local-storage');

Vue.use(VueI18n);

const setI18nLanguage = (language) => {
	i18n.locale = language;
	document.querySelector('html').setAttribute('lang', language);

	ls.set(langKey, language);

	return language;
};

export const getLanguage = () => {
	const storedLang = ls.get(langKey);
	const language = storedLang || window.navigator.userLanguage || window.navigator.language;

	return defaultLanguages.includes(language) ? language : defaultLanguage;
};

export const setLanguage = () => loadLanguageAsync(getLanguage());

export const loadLanguageAsync = (language) => {
	if (i18n.locale !== language) {
		return import(`./locales/${language}`).then((msgs) => {
			i18n.setLocaleMessage(language, msgs.default);

			return setI18nLanguage(language);
		});
	}
	return Promise.resolve(language);
};

const langKey = 'lang';
const defaultLanguages = ['en', 'es'];
const defaultLanguage = 'en';
const actualLanguage = getLanguage();

const messages = require(`./locales/${actualLanguage}.json`);

export const i18n = new VueI18n({
	locale: actualLanguage,
	fallbackLocale: defaultLanguage,
	silentTranslationWarn: true,
	messages: {
		[actualLanguage]: messages
	}
});
