const { defineConfig } = require('@vue/cli-service');
const path = require('path');

module.exports = defineConfig({
	transpileDependencies: true,
	publicPath: './',
	devServer: {
		hot: true,
		host: 'binaccle.org',
		https: true,
		port: 3002
	},
	css: {
		loaderOptions: {
			sass: {
				additionalData: `@import "@/sass/_variables.scss";`
			}
		}
	}
});
